발화자 문장 끝점 찾는 방법

※ EPD (Epd_Point_Detection)
- BERT 학습모델을 이용하여 발화자의 문장 끝이 어디인지 찾아내는 문제

※ 학습 데이터
- 데이터셋: 한양건설 녹취록 + 세종 말뭉치 + 네이버 Clova_call_data
  Train 45,863
  validate 13,087 
  test 6,490

- 실험결과
  
- [ ] accuracy: 0.96
 
- [ ] precision: 0.88
  
- [ ] recall(eos_accuracy): 0.97
- [ ] no_eos_accuracy(문장의 끝점이 아닌 경우): 0.96

※ 학습 방법: 띄어쓰기기준 어절 구간 별 단어 학습 

※ 학습 샘플: '42_0603_972_0_03380_04.wav': '7시쯤 가지러 갈게요.',

 (training #1) 7시쯤

 (training #2) 7시쯤 가지러

 (training #3) 7시쯤 가지러 갈게요.

 1문장에 대해서 총 3번 학습하여 문장 EPD 학습
